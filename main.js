function editMsg(message) {
    let textarea = message.childNodes[0];
    let buttonEdit = message.childNodes[1];

    if (textarea.disabled) {
        textarea.disabled = false;
        
        buttonEdit.textContent = "Confirmar";
        buttonEdit.classList.replace("edit-button", "confirm-button");
        return;
    }

    textarea.disabled = true;

    buttonEdit.textContent = "Editar";
    buttonEdit.classList.replace("confirm-button", "edit-button");
}

function deleteMsg(message) {
    message.remove();
}

function sendMsg() {
    let message = document.createElement("div");
    message.classList.add("msg");

    let textarea = document.createElement("textarea");
    textarea.value = msg.value;
    textarea.rows = 5;
    textarea.cols = 50;
    textarea.disabled = true;
    message.appendChild(textarea);

    msg.value = "";

    let buttonEdit = document.createElement("button");
    buttonEdit.classList.add("edit-button")
    buttonEdit.type = "button";
    buttonEdit.textContent = "Editar";
    buttonEdit.addEventListener("click", ()=>(editMsg(message)));
    message.appendChild(buttonEdit);

    let buttonDelete = document.createElement("button");
    buttonDelete.classList.add("delete-button")
    buttonDelete.type = "button";
    buttonDelete.textContent = "Deletar";
    buttonDelete.addEventListener("click", ()=>(deleteMsg(message)));
    message.appendChild(buttonDelete);

    msgLog.appendChild(message);
}


console.log("script carregado")
var msgLog = document.getElementById("msg-log");
var msg = document.getElementById("msg");
var send = document.getElementById("send-button");
send.addEventListener("click", sendMsg);
